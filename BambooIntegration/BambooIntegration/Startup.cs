﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BambooIntegration.Startup))]
namespace BambooIntegration
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
